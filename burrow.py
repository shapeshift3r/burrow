#!/usr/bin/env python

import os
import click
import config
import pika
import datetime
import json
import logging
import sys
import time
from pprint import pprint

CONFIG_FILE = 'config.yml'

RABBITMQ_QUEUE_NAME = 'burrow'
RABBITMQ_QUEUE_ROUTING_KEY = '#'

logger = None
logfile = None
rabbitmq_connection = None
rabbitmq_channel = None

def connect_to_rabbitmq(exchange=None, queue=False):
    global rabbitmq_connection, rabbitmq_channel, configuration

    if not rabbitmq_connection:
        rabbitmq_connection = pika.BlockingConnection(pika.ConnectionParameters(
            host=configuration['rabbitmq']['host']))

    if not rabbitmq_channel:
        rabbitmq_channel = rabbitmq_connection.channel()
        rabbitmq_channel.confirm_delivery()

    if exchange:
        rabbitmq_channel.exchange_declare(
            exchange=exchange,
            type='topic'
        )

        if queue:
            rabbitmq_channel.queue_declare(
                queue=RABBITMQ_QUEUE_NAME,
                durable=True
            )
            rabbitmq_channel.queue_bind(
                exchange=exchange,
                queue=RABBITMQ_QUEUE_NAME,
                routing_key=RABBITMQ_QUEUE_ROUTING_KEY
            )

def setup_logger(verbose=False):
    global logger

    logLevel = logging.INFO

    if verbose:
        logLevel = logging.DEBUG

    logger = logging.getLogger('logger')
    logger.setLevel(logLevel)
    format = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")

    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(format)
    logger.addHandler(ch)

    fh = logging.FileHandler(configuration['logfile'])
    fh.setFormatter(format)
    logger.addHandler(fh)

@click.group()
def cli():
    pass

@click.command()
@click.argument('exchange')
@click.argument('routing_key')
@click.argument('message')
@click.option('-v', '--verbose', is_flag=True)
def publish( exchange, routing_key, message, verbose ):
    """Publish a message

        eg: burrow.py publish EXCHANGE ROUTE_KEY MESSAGE
    """
    setup_logger(verbose)

    try:
        connect_to_rabbitmq(exchange=exchange)

        sent = rabbitmq_channel.basic_publish(exchange=exchange,
                                      routing_key=routing_key,
                                      body=message,
                                      properties=pika.BasicProperties(
                                          content_type='application/json',
                                          delivery_mode=2
                                      ),
                                      mandatory=True)

        if sent:
            logger.info("Sent: %s" % message_to_string(exchange, routing_key, message))
        else:
            logger.error("NOT Sent: %s" % message_to_string(exchange, routing_key, message))

    except pika.exceptions.AMQPConnectionError as e:
        logger.critical("Could not connect. %s" % e)
        sys.exit()

    except Exception as e:
        if rabbitmq_connection:
            rabbitmq_connection.close()

        logger.critical(" %s" % e)
        sys.exit()

    rabbitmq_connection.close()

@click.command()
@click.argument('filename')
@click.option('-v', '--verbose', is_flag=True)
@click.option('-l', '--live', is_flag=True, help='If body is json and contains timestamp key, timestamp is set to now.')
@click.option('-d', '--delay', default=0, help='Delay between sending messages in seconds.')
def replay( filename, verbose, live, delay ):
    """Replay a file

        eg: burrow.py replay burrow-test-2015-05-03.log

        --live will replace "timestamp": "2015-06-17 11:00:00" with datetime.datetime.today() in message
        --delay adds a delay in seconds between sending messages to the queue.  1 = 1s, 0.1 = 100ms
    """
    global configuration

    setup_logger(verbose)

    try:
        connect_to_rabbitmq()

        with open( "%s/%s" % ( configuration['output']['dir'], filename ) ) as file:
            for line in file:

                event = json.loads( line )

                if live:
                    try:
                        # try decode body as json, if fails just carry on
                        body = json.loads( event['body'] )

                        if 'timestamp' in body:
                            body['timestamp'] = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
                            event['body'] = json.dumps(body)

                    except Exception, e:
                        pass

                sent = rabbitmq_channel.basic_publish(exchange=event['exchange'],
                                              routing_key=event['routing_key'],
                                              body=event['body'],
                                              properties=pika.BasicProperties(
                                                  content_type='application/json',
                                                  delivery_mode=2
                                              ),
                                              mandatory=True)

                if sent:
                    logger.info("Sent: %s" % message_to_string(event['exchange'], event['exchange'], event['body']))
                else:
                    logger.error("NOT Sent: %s" % message_to_string(event['exchange'], event['exchange'], event['body']))

                time.sleep(delay)

    except pika.exceptions.AMQPConnectionError as e:
        logger.critical("Could not connect. %s" % e)
        sys.exit()

    except Exception, e:
        if rabbitmq_connection:
            rabbitmq_connection.close()
        logger.critical(" %s" % e)
        sys.exit()

    rabbitmq_connection.close()

@click.command()
@click.option('-v', '--verbose', is_flag=True)
@click.argument('exchange')
def consume(exchange, verbose):
    """Consume device status updates off a RabbitMQ queue.

        eg: device.py consume
    """
    global output_file

    setup_logger(verbose)

    output_file = output_file.replace( 'EXCHANGE', exchange )

    try:
        logger.info("Connecing to RabbitMQ")
        connect_to_rabbitmq(exchange, queue=True)

        logger.info("Setup queue consumer callback")
        rabbitmq_channel.basic_consume(
            consume_message_callback,
            queue=RABBITMQ_QUEUE_NAME
        )

    except Exception as e:
        logger.critical(" %s" % e)
        sys.exit()

    try:
        logger.info("* Waiting for messages on exchange '%s'" % exchange)
        rabbitmq_channel.start_consuming()
    except KeyboardInterrupt:
        rabbitmq_channel.stop_consuming()
    except Exception as e:
        logger.critical(" %s" % e)
        sys.exit()

    rabbitmq_connection.close()

def consume_message_callback(channel, method, properties, body):
    global logfile

    logger.debug( message_to_string( method.exchange, method.routing_key, body ) )

    try:
        # assuming body is a json string, ( or int i guess )
        line = '{"timestamp":"%s","exchange":"%s","routing_key":"%s","body":%s}\n' % ( datetime.datetime.today(), method.exchange, method.routing_key, json.dumps(body) )

        output = open( output_file, 'a' )
        output.write( line )
        output.close()
        channel.basic_ack(delivery_tag = method.delivery_tag)

    except Exception as e:
        logger.critical( " %s, %s" % ( e, message_to_string( method.exchange, method.routing_key, body ) ) )
        channel.basic_reject(delivery_tag = method.delivery_tag)
        sys.exit()

def message_to_string( exchange, routing_key, body ):
    return "exchange: %s, routing_key: %s, body: %s" % ( exchange, routing_key, body )

@click.command()
def list():
    print "Burrow replay files available:"
    for filename in os.listdir( configuration['output']['dir'] ):
        if filename.startswith('burrow-') and filename.endswith('.log'):
            print filename

cli.add_command(replay)
cli.add_command(publish)
cli.add_command(consume)
cli.add_command(list)

if __name__ == '__main__':

    if not os.path.exists(CONFIG_FILE):
        click.echo('%s not found.' % CONFIG_FILE)
        exit( 'Exiting...' )

    configuration = config.Config(CONFIG_FILE).get()

    output_file = "%s/%s" % ( configuration['output']['dir'], configuration['output']['filename'])
    output_file = output_file.replace( 'DATETIME', datetime.datetime.today().strftime('%Y-%m-%d') )

    cli()
