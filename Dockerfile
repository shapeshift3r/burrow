FROM python:2.7

ADD requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

ADD . /code

WORKDIR /code
EXPOSE 5000

ENV APP_NAME burrow

ADD run.sh /tmp/run.sh

ENTRYPOINT [ "sh", "/tmp/run.sh" ]

CMD [ "--help" ]
