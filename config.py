import os
import yaml

class Config:
    config = {}

    def __init__(self, file=None):
        self.from_yaml( file )

    def from_yaml(self, config_file):
        with open(config_file, 'r') as f:
            config = yaml.load(f)

        self.config = replaceEnvVars(config)

    def get(self):
        return self.config

def replaceEnvVars( config ):
    newConfig = {}

    for key, value in config.items():
        if( type( value ) is dict ):
            newConfig[key] = replaceEnvVars( value )
        else:
            if '||' in value:
                value = os.getenv(
                    value.split('||', 1)[0],
                    value.split('||', 1)[1]
                )
            newConfig[key] = value

    return newConfig
